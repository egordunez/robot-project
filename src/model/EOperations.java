/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * Contains all the possibles movements from the source of commands
 * @author hipnosapo
 */
public enum EOperations {
    UP("UP"),
    RI("RI"),
    DO("DO"),
    LE("LE"),
    FW("FW"),
    BA("BA");
    
    private String operation;

    EOperations(String operation) {
        this.operation = operation;
    }

    public String getOperation() {
        return operation;
    }
}
