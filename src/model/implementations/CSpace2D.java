/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.implementations;

import exceptions.BusyPositionEx;
import exceptions.ImposiblePositionEx;
import model.interfaces.ISpace;
import model.interfaces.MovementListener;

/**
 * Implement the interfaces of Space and to subscribe to the movement of an Robot
 * @author hipnosapo
 */
public class CSpace2D implements ISpace, MovementListener {

    protected boolean[][] bussyPositions;
    protected int xLength;
    protected int yLength;

    public CSpace2D() {
        this.xLength = 5;
        this.yLength = 5;
        bussyPositions = new boolean[xLength][yLength];
    }

    public CSpace2D(int xLength, int yLength) {
        this.xLength = xLength;
        this.yLength = yLength;
        bussyPositions = new boolean[xLength][yLength];
    }

    /**
     * Receive an array with coordinates for 2D movement
     * If the position is inside of the limits of the space return true,
     * throw an exception else
     * @param positions
     * @return
     * @throws exceptions.ImposiblePositionEx
     */
    @Override
    public boolean isRigthPosition(Integer... positions) throws ImposiblePositionEx {
        if(positions[0] < 0 || positions[0] > xLength){
            throw new ImposiblePositionEx(xLength,positions[0]);
        }else if(positions[1] < 0 || positions[1] > yLength){
            throw new ImposiblePositionEx(yLength,positions[1]);
        }else{
            return true;
        }
    }

    /**
     * Receive an array with coordinates
     * If the position is empty return true,
     * throw an exception else
     * @param positions
     * @return
     * @throws exceptions.BusyPositionEx
     */
    @Override
    public boolean isEmptyPosition(Integer... positions) throws BusyPositionEx,ImposiblePositionEx{
        if(positions[0] <= 0 || positions[1] <= 0){
            throw new ImposiblePositionEx(0,positions[0]);
        }else if(bussyPositions[positions[0]-1][positions[1]-1]){
            throw new BusyPositionEx(positions);
        }else{
            return true;
        }
    }

    /**
     * Receive an arrays with coordinates of old position, new position
     *
     * @param oldPosition
     * @param newPosition
     */
    @Override
    public void onMovementListener(int[] oldPosition, int[] newPosition) {
        bussyPositions[oldPosition[0]-1][oldPosition[1]-1] = false;
        bussyPositions[newPosition[0]-1][newPosition[1]-1] = true;
    }

}
