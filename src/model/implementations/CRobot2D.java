/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.implementations;

import java.util.ArrayList;
import java.util.List;
import model.interfaces.IMovement2D;
import model.interfaces.MovementListener;

/**
 * Implements the interfaces of movements in 2D
 * @author hipnosapo
 */
public class CRobot2D implements IMovement2D {

    protected int positionX;
    protected int xStepSize;
    protected int positionY;
    protected int yStepSize;
    protected List<MovementListener> movementListeners;

    public CRobot2D() {
        this.positionX = 1;
        this.xStepSize = 1;
        this.positionY = 1;
        this.yStepSize = 1;
    }

    public CRobot2D(int positionX, int positionY, int xStepSize, int yStepSize) {
        this.positionX = positionX;
        this.xStepSize = xStepSize;
        this.positionY = positionY;
        this.yStepSize = yStepSize;
    }

    /**
     * Do one less step in Y
     */
    @Override
    public void moveUp() {
        positionY -= yStepSize;
        notifyMovement(new int []{positionX,positionY + yStepSize},new int []{positionX,positionY});
    }

    /**
     * Do one more step in Y
     */
    @Override
    public void moveDown() {
        positionY += yStepSize;
        notifyMovement(new int []{positionX,positionY - yStepSize},new int []{positionX,positionY});
    }

    /**
     * Return the position in Y of the robot
     *
     * @return
     */
    @Override
    public int getPositionY() {
        return positionY;
    }

    /**
     * Return the size of the step in Y
     * @return 
     */
    @Override
    public int getYStepSize() {
        return yStepSize;
    }

    /**
     * Change the size of the step in Y
     * @param position 
     */
    @Override
    public void setYStepSize(int position) {
        this.yStepSize = position;
    }

    /**
     * Do one less step in X
     */
    @Override
    public void moveLeft() {
        positionX -= xStepSize;
        notifyMovement(new int []{positionX + xStepSize,positionY},new int []{positionX,positionY});
    }

    /**
     * Do one more step in X
     */
    @Override
    public void moveRigth() {
        positionX += xStepSize;
        notifyMovement(new int []{positionX - xStepSize,positionY},new int []{positionX,positionY});
    }

    /**
     * Return position on X of the robot
     *
     * @return
     */
    @Override
    public int getPositionX() {
        return positionX;
    }

    /**
     * Return the size of step in X
     *
     * @return
     */
    @Override
    public int getXStepSize() {
        return xStepSize;
    }

    /**
     * Change the size of the step in X
     * @param position 
     */
    @Override
    public void setXStepSize(int position) {
        this.xStepSize = position;
    }

    /**
     * Notify to all subscribers when the robot do a movement
     * @param oldPosition an array with the previous coordinates
     * @param newPosition an array with the next coordinates
     */
    private void notifyMovement(int[] oldPosition, int[] newPosition) {
        movementListeners.forEach(m -> {
            m.onMovementListener(oldPosition, newPosition);
        });
    }

    /**
     * Subscribe to the movement of the robot
     * @param movementListener 
     */
    @Override
    public void addMovementListener(MovementListener movementListener) {
        if (movementListeners == null) {
            movementListeners = new ArrayList<>();
        }
        movementListeners.add(movementListener);
    }

    /**
     * Unsubscribe to the movement of the robot
     * @param movementListener 
     */
    @Override
    public void removeMovementListener(MovementListener movementListener) {
        if (movementListeners != null) {
            movementListeners.remove(movementListener);
        }
        
    }
}
