/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.implementations;

import exceptions.BusyPositionEx;
import model.interfaces.ACOperator;
import model.interfaces.IMovement2D;
import exceptions.ImposiblePositionEx;
import exceptions.WrongOperationEx;
import java.util.Arrays;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import model.EOperations;
import model.interfaces.ISpace;
import model.interfaces.MovementListener;

/**
 * Override the methods from the template class to work with 2D Robots and Space
 * @author hipnosapo
 */
public class COperator2D extends ACOperator{

    protected Queue<EOperations> eOperations;
    protected ISpace space;
    protected IMovement2D robot;

    public COperator2D(ISpace space,IMovement2D robot) {
        this.robot = robot;
        this.robot.addMovementListener((MovementListener) space);
        this.space = space;
    }

    /**
     * Transform all the text operations from an string into right operations
     * @param operations
     * @throws WrongOperationEx 
     */
    @Override
    protected void loadOperation(String operations) throws WrongOperationEx{
        if (eOperations == null) {
            eOperations = new LinkedBlockingQueue<>();
        }
        for (String op : operations.split(",")) {
            try {
                eOperations.offer(Arrays.asList(EOperations.values()).stream().filter(o -> op.equals(o.toString())).findFirst().get());
            } catch (NullPointerException e) {
                //if does not found a posible operation
                throw new WrongOperationEx(op);
            }
        }
    }

    /**
     * Say if exists operations into the Queue
     * @return 
     */
    @Override
    protected boolean existsOperation() {
        return !eOperations.isEmpty();
    }

    /**
     * Command to the robot made the correspondent operation into the Queue
     * @throws ImposiblePositionEx
     * @throws BusyPositionEx 
     */
    @Override
    protected void nextOperation() throws ImposiblePositionEx,BusyPositionEx {
        
        //Obtain current position
        int xPosition = robot.getPositionX();
        int yPosition = robot.getPositionY();
        EOperations nexOp = eOperations.poll();
        switch (nexOp) {
            case UP:
                yPosition -= robot.getYStepSize();
                if(space.isRigthPosition(xPosition,yPosition) && space.isEmptyPosition(xPosition,yPosition)){
                    robot.moveUp();
                }
                break;
            case RI:
                xPosition += robot.getXStepSize();
                if(space.isRigthPosition(xPosition,yPosition) && space.isEmptyPosition(xPosition,yPosition)){
                    robot.moveRigth();
                }
                break;
            case DO:
                yPosition += robot.getYStepSize();
                if(space.isRigthPosition(xPosition,yPosition) && space.isEmptyPosition(xPosition,yPosition)){
                    robot.moveDown();
                }
                break;
            case LE:
                xPosition -= robot.getXStepSize();
                if(space.isRigthPosition(xPosition,yPosition) && space.isEmptyPosition(xPosition,yPosition)){
                    robot.moveLeft();
                }
                break;
        }
    }    
}

