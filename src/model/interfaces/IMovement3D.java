/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.interfaces;

/**
 * Contract to movement in 3D
 * @author hipnosapo
 */
public interface IMovement3D extends IMovement2D{

    void moveForward();

    void moveBack();

    int getPositionZ();
    
    int getZStepSize();

    void setZStepSize(int position);
}
