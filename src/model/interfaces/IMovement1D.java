/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.interfaces;

/**
 * Contract to movement in 1D
 * @author hipnosapo
 */
public interface IMovement1D {

    void moveLeft();

    void moveRigth();

    int getPositionX();
    
    int getXStepSize();

    void setXStepSize(int position);
    
    void addMovementListener(MovementListener movementListener);
    
    void removeMovementListener(MovementListener movementListener);
            
}
