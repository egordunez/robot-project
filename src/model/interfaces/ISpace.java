/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.interfaces;

import exceptions.BusyPositionEx;
import exceptions.ImposiblePositionEx;

/**
 * Contract to space
 * @author hipnosapo
 */
public interface ISpace {
    
    boolean isRigthPosition(Integer... positions) throws ImposiblePositionEx;
    
    boolean isEmptyPosition(Integer... positions) throws BusyPositionEx,ImposiblePositionEx;
    
}
