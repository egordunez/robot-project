/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.interfaces;

import exceptions.BusyPositionEx;
import exceptions.ImposiblePositionEx;
import exceptions.WrongOperationEx;

/**
 *
 * @author hipnosapo
 */
public abstract class ACOperator {
    
    /**
     * The template method that know how operate an movement
     * @param operations
     * @throws WrongOperationEx
     * @throws ImposiblePositionEx
     * @throws BusyPositionEx 
     */
    public final void operate(String operations)throws WrongOperationEx,ImposiblePositionEx,BusyPositionEx{
        loadOperation(operations);
        while(existsOperation()){
            nextOperation();
        }
    }

    protected abstract void loadOperation(String operations)throws WrongOperationEx;

    protected abstract boolean existsOperation();

    protected abstract void nextOperation() throws ImposiblePositionEx, BusyPositionEx;
    
}
