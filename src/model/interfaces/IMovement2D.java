/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.interfaces;

/**
 * Contract to movement in 2D
 * @author hipnosapo
 */
public interface IMovement2D extends IMovement1D{
    
    void moveUp();

    void moveDown();

    int getPositionY();
    
    int getYStepSize();

    void setYStepSize(int position);
}
