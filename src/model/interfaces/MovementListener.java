/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.interfaces;

/**
 * Contract to listen movements
 * @author hipnosapo
 */
public interface MovementListener {
    void onMovementListener(int [] oldPosition,int [] newPosition);
}
