/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

import model.EOperations;

/**
 * Notify when an operation is wrong
 * @author hipnosapo
 */
public class WrongOperationEx extends Exception{
    private final String operation;

    public WrongOperationEx(String operation) {
        this.operation = operation;
    }

    @Override
    public String getMessage() {
        StringBuilder operations = new StringBuilder("[");
        for(EOperations c : EOperations.values()){
            operations.append(c.toString() + ",");
        }
        operations.deleteCharAt(operations.length()-1);
        operations.append("]");
        return "The operation " + operation + " is not between " + operations.toString();
    }
    
    
}
