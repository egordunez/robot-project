/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 * Notify when a robot will move to an invalid position
 * @author hipnosapo
 */
public class ImposiblePositionEx extends Exception{
    
    private final int length;
    private final int position;
    

    public ImposiblePositionEx(int length, int position) {
        this.length = length;
        this.position = position;
    }
    
    

    @Override
    public String getMessage() {
        if(position <= 0){
            return "The position is imposible because: " + position + " <= 0"; 
        }else{
            return "The position is imposible because: " + position + ">" + length; 
        }
    }
    
    
}

