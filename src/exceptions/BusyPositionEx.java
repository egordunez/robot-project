/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;


/**
 * Notify when an position is busy by another
 * @author hipnosapo
 */
public class BusyPositionEx extends Exception{
    
    private final Integer[] coordinates;

    public BusyPositionEx(Integer... coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public String getMessage() {
        StringBuilder output = new StringBuilder("(");
        for(int c : coordinates){
            output.append(c + ",");
        }
        output.deleteCharAt(output.length()-1);
        output.append(")");
        return "The position " + output + " is busy";
    }
    
    
}
