/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package for_test;

import exceptions.BusyPositionEx;
import exceptions.ImposiblePositionEx;
import exceptions.WrongOperationEx;
import java.util.Queue;
import model.EOperations;
import model.implementations.COperator2D;
import model.interfaces.IMovement2D;
import model.interfaces.ISpace;

/**
 * This class exposes all methods of the super class for testing
 * @author hipnosapo
 */
public class COperator2DForTest extends COperator2D{

    public COperator2DForTest(ISpace space, IMovement2D robot) {
        super(space, robot);
    }

    public Queue<EOperations> geteOperations() {
        return eOperations;
    }

    public ISpace getSpace() {
        return space;
    }

    public IMovement2D getRobot() {
        return robot;
    }

    @Override
    public void nextOperation() throws ImposiblePositionEx, BusyPositionEx {
        super.nextOperation(); 
    }

    @Override
    public void loadOperation(String operations) throws WrongOperationEx {
        super.loadOperation(operations);
    }
    
    
    
}
