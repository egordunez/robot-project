/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotproject;

import exceptions.BusyPositionEx;
import exceptions.ImposiblePositionEx;
import exceptions.WrongOperationEx;
import file_access.CFileAccess;
import model.implementations.CRobot2D;
import model.implementations.COperator2D;
import model.implementations.CSpace2D;
import model.interfaces.ACOperator;
import model.interfaces.IMovement2D;
import model.interfaces.ISpace;

/**
 *
 * @author hipnosapo
 */
public class RobotProgram{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        try {
            IMovement2D robot = new CRobot2D();
            ISpace space = new CSpace2D();
            ACOperator operator = new COperator2D(space,robot);
            robot.addMovementListener((int[] oldPosition, int[] newPosition) -> {
                StringBuilder output = new StringBuilder("(");
                output.append(newPosition[0]);
                output.append(",");
                output.append(newPosition[1]);
                output.append(")");
                System.out.println(output.toString());
            });
            operator.operate(CFileAccess.readFile(args.length > 0 ? args[0] : CFileAccess.COMMAND_PATH));
        } catch (WrongOperationEx ex) {
            System.err.println(ex.getMessage());
        } catch (ImposiblePositionEx ex) {
            System.err.println(ex.getMessage());
        } catch (BusyPositionEx ex) {
            System.err.println(ex.getMessage());
        }
    }

}
