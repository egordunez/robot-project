/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package file_access;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author hipnosapo
 */
public class CFileAccess {
    
    /**
     * Used for test
     */
    public static final String COMMAND_PATH = "test/file_access/commands.txt";

    /**
     * Returns the string contained in the file received by parameters
     *
     * @param path
     * @return
     */
    public static String readFile(String path) {
        String fileContent = "";
        try (FileInputStream input = new FileInputStream(path)) {
            BufferedReader br = new BufferedReader(new InputStreamReader(input));
            String strLine;
            //Read File Line By Line
            while ((strLine = br.readLine()) != null) {
                fileContent += strLine;
            }

        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        
        return fileContent;
    }

}
