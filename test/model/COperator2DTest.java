/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import for_test.COperator2DForTest;
import model.implementations.CRobot2D;
import java.util.concurrent.LinkedBlockingQueue;
import model.implementations.CSpace2D;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author hipnosapo
 */
public class COperator2DTest {

    private COperator2DForTest operator;
    private CRobot2D robot2D;
    private CSpace2D space;
    protected LinkedBlockingQueue<EOperations> expectedOperations;

    public COperator2DTest() {
    }

    @Before
    public void setUp() throws Exception {
        robot2D = new CRobot2D();
        space = new CSpace2D();
        operator = new COperator2DForTest(space, robot2D);

        expectedOperations = new LinkedBlockingQueue<>();
        expectedOperations.offer(EOperations.RI);
        expectedOperations.offer(EOperations.RI);
        expectedOperations.offer(EOperations.DO);
    }

    /**
     * Test of loadOperation method, of class COperator2D.
     * @throws java.lang.Exception
     */
    @Test
    public void testLoadOperation() throws Exception {
        operator.loadOperation("RI,RI,DO");
        assertArrayEquals(expectedOperations.toArray(), operator.geteOperations().toArray());
    }

    /**
     * Test of nextOperation method, of class COperator2D.
     * @throws java.lang.Exception
     */
    @Test
    public void testNextOperation() throws Exception {
        //operator.loadOperation("RI,RI,DO");
        
        operator.loadOperation("RI");
        operator.nextOperation();
        assertEquals(robot2D.getPositionX(), 2);
        
        operator.loadOperation("DO");
        operator.nextOperation();
        assertEquals(robot2D.getPositionY(), 2);
        
        operator.loadOperation("LE");
        operator.nextOperation();
        assertEquals(robot2D.getPositionX(), 1);
        
        operator.loadOperation("UP");
        operator.nextOperation();
        assertEquals(robot2D.getPositionY(), 1);
    }
}
