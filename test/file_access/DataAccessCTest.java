/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package file_access;

import file_access.CFileAccess;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author hipnosapo
 */
public class DataAccessCTest {
    
    private String spectedReadFileNotFail;
    
    public DataAccessCTest() {
    }
    
    @Before
    public void setUp() {
        spectedReadFileNotFail = "RI,RI,DO";
    }

    /**
     * Test file read correctly
     */
    @Test
    public void testReadFileNotFail() {
        assertEquals(spectedReadFileNotFail,CFileAccess.readFile(CFileAccess.COMMAND_PATH));
    }
    
}
